#[cfg(feature = "data-stream")]
use data_stream::{
    FromStream, ToStream,
    collections::SizeSettings,
    from_stream,
    numbers::{EndianSettings, LittleEndian},
    to_stream,
};
use num_traits::Zero;
use vector_basis::{Bases as _, Basis};

use std::fmt::Debug;

#[cfg(feature = "data-stream")]
use std::io::{Error, ErrorKind, Read, Write};

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum PositionSpace {
    Absolute,
    Relative,
}

#[derive(Copy, Clone, Debug)]
pub struct InputData<V> {
    pub index: usize,
    pub grab: bool,
    pub special: bool,
    pub position_space: PositionSpace,
    pub axes: V,
    changed: bool,
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S>> ToStream<S> for InputData<V> {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        let mut bits: u8 = 0;
        if self.grab {
            bits |= 1;
        }
        if self.special {
            bits |= 2;
        }
        if self.position_space == PositionSpace::Relative {
            bits |= 4;
        }

        to_stream::<S, _, _>(&self.index, writer)?;
        to_stream::<LittleEndian, _, _>(&bits, writer)?;
        to_stream(&self.axes, writer)?;

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S>> FromStream<S> for InputData<V> {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let index = from_stream::<S, _, _>(reader)?;
        let bits: u8 = from_stream::<LittleEndian, _, _>(reader)?;
        Ok(Self {
            index,
            grab: bits & 1 != 0,
            special: bits & 2 != 0,
            position_space: if bits & 4 == 0 {
                PositionSpace::Absolute
            } else {
                PositionSpace::Relative
            },
            axes: from_stream(reader)?,
            changed: false,
        })
    }
}

impl<V> InputData<V> {
    pub fn new(index: usize) -> Self
    where
        V: Zero,
    {
        Self {
            index,
            grab: false,
            special: false,
            position_space: PositionSpace::Relative,
            axes: V::zero(),
            changed: false,
        }
    }

    pub fn changed(&mut self) -> bool {
        let result = self.changed;
        self.changed = false;
        result
    }

    pub fn set_grab(&mut self, value: bool) {
        self.grab = value;
        self.changed = true;
    }

    pub fn set_special(&mut self, value: bool) {
        self.special = value;
    }

    pub fn set_axis<const I: usize>(&mut self, value: V::Scalar)
    where
        V: Basis<I>,
    {
        self.position_space = PositionSpace::Relative;
        *self.axes.bases_mut::<I>() = value;
    }

    pub fn set_axes(&mut self, value: V) {
        self.position_space = PositionSpace::Absolute;
        self.axes = value;
    }
}

#[derive(Debug)]
pub enum Request {
    Reload,
    TogglePause,
    StartSlowdown,
}

#[cfg(feature = "data-stream")]
impl<S: EndianSettings> ToStream<S> for Request {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        use Request::*;
        to_stream::<S, _, _>(
            &match self {
                Reload => 0u8,
                TogglePause => 1u8,
                StartSlowdown => 2u8,
            },
            writer,
        )
    }
}

#[cfg(feature = "data-stream")]
impl<S: EndianSettings> FromStream<S> for Request {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        use Request::*;
        match from_stream::<S, u8, _>(reader)? {
            0 => Ok(Reload),
            1 => Ok(TogglePause),
            2 => Ok(StartSlowdown),
            _ => Err(Error::new(ErrorKind::InvalidInput, "Invalid element type")),
        }
    }
}

#[derive(Debug)]
pub enum InputMessage<V> {
    Request(Request),
    Input(InputData<V>),
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S>> ToStream<S> for InputMessage<V> {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        use InputMessage::*;
        match self {
            Request(request) => {
                to_stream::<S, _, _>(&0u8, writer)?;
                to_stream::<S, _, _>(request, writer)?;
            }
            Input(input) => {
                to_stream::<S, _, _>(&1u8, writer)?;
                to_stream::<S, _, _>(input, writer)?;
            }
        }

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S>> FromStream<S> for InputMessage<V> {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        use InputMessage::*;
        match from_stream::<S, u8, _>(reader)? {
            0 => Ok(Request(from_stream::<S, _, _>(reader)?)),
            1 => Ok(Input(from_stream::<S, _, _>(reader)?)),
            _ => Err(Error::new(ErrorKind::InvalidInput, "Invalid element type")),
        }
    }
}
